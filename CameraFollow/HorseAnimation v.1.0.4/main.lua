-- Project: HorseSprite
--
-- Date: September 27, 2010
--
-- Version: 1.0
--
-- File name: main.lua
--
-- Code type: Game Edition Sample Code
--
-- Author: Japan Corona Group
--
-- Demonstrates: Use of optimized sprite sheets, loaded using newSpriteSheetFromData
--
-- File dependencies: none
--
-- Target devices: Simulator (results in Console)
--
-- Limitations:
--
-- Update History:
--
-- Comments: 
--
-- Sample code is MIT licensed, see https://www.coronalabs.com/links/code/license
-- Copyright (C) 2010 Corona Labs Inc. All Rights Reserved.
--
--	Supports Graphics 2.0
---------------------------------------------------------------------------------------

local centerX = display.contentCenterX
local centerY = display.contentCenterY
local _W = display.contentWidth
local _H = display.contentHeight

-- Define reference points locations anchor ponts
local TOP_REF = 0
local BOTTOM_REF = 1
local LEFT_REF = 0
local RIGHT_REF = 1
local CENTER_REF = 0.5

display.setStatusBar(display.HiddenStatusBar)

local perspective = require("camera.perspective")

--------------------------------------------------------------------------------
-- Build Camera
--------------------------------------------------------------------------------
local camera = perspective.createView()


display.setDefault( "anchorX", 0.0 )	-- default to TopLeft anchor point for new objects
display.setDefault( "anchorY", 0.0 )

local background = display.newImage("background.png", 0, 0 ) 
local moon = display.newImage("moon.png", 22, 19) 
--[[




local mountain_big = display.newImage("mountain_big.png", 132-240, 92) 
local mountain_big2 = display.newImage("mountain_big.png", 132-720, 92) 
local mountain_sma = display.newImage("mountain_small.png", 84, 111)
local mountain_sma2 = display.newImage("mountain_small.png", 84 - 480, 111)

local tree_s = display.newImage("tree_s.png", 129-30, 151) 
local tree_s2 = display.newImage("tree_s.png", 270 + 10,151)
local tree_l = display.newImage("tree_l.png", 145, 131) 

local tree_s3 = display.newImage("tree_s.png", 129-30 - 320, 151) 
local tree_s4 = display.newImage("tree_s.png", 270 + 10 - 320,151)
local tree_l2 = display.newImage("tree_l.png", 145 - 320, 131) 

local tree_s5 = display.newImage("tree_s.png", 129 - 30 - 640, 151) 
local tree_s6 = display.newImage("tree_s.png", 270 + 10 - 640,151)
local tree_l3 = display.newImage("tree_l.png", 145 - 640, 131) 

local fog = display.newImage("Fog.png", 0, 214) 
local fog2 = display.newImage("Fog.png",-480,214)


--]]
background.x = 0
background.y = 0
background.anchorY=0
background.anchorX=0
-- The following demonstrates using the new image sheet data format 
-- where uma_old.lua has been migrated to the new format (uma.lua)
local options =
{
	frames = require("uma").frames,
}

-- The new sprite API
local umaSheet = graphics.newImageSheet( "uma.png", options )
local spriteOptions = { name="uma", start=1, count=8, time=1000 }
local spriteInstance = display.newSprite( umaSheet, spriteOptions )

spriteInstance.anchorX = RIGHT_REF
spriteInstance.anchorY = BOTTOM_REF
spriteInstance.x = _W*0.5
spriteInstance.y = _H*0.5
spriteInstance.anchorY=0
spriteInstance.anchorX=0

local tree_l_sugi = display.newImage("tree_l_sugi.png", 23, 0) 
local tree_l_take = display.newImage("tree_l_take.png", 151, 0) 

local panning
panning = display.newRect( spriteInstance.x, spriteInstance.y, spriteInstance.width, spriteInstance.height )
panning.alpha=0.7--should be 0.01

local cameraXAdditional = -spriteInstance.width*1.3 
local cameraYAdditional =  0


--local rakkan = display.newImage("rakkan.png", 19, 217) 
--local rakkann = display.newImage("rakkann.png", 450, 11) 

--local wallOutSide = display.newRect(480,0,200,320)
--wallOutSide:setFillColor(0,0,0)

--local wallOutSide2 = display.newRect(-200,0,200,320)
--wallOutSide2:setFillColor(0,0,0)

camera:setParallax(1.2, 1.1, 1, 0.6, 0.5)
       --camera:setParallax(f,    g,  cave, h,  com,  cl,   bk)
           --camera:setBounds(fullWidth*0.5, levelWidth-fullWidth*0.5, -fullHeight, fullHeight*0.5)
           --change to 
local boundaries = {}           
		boundaries.bx1=0--_W*0.5
        boundaries.bx2= _W--levelWidth-_W*0.5
        boundaries.by1=-_H*5
        boundaries.by2=_H*0.55

camera:setBounds(boundaries.bx1,boundaries.bx2, boundaries.by1,boundaries.by2 )  

camera:add(tree_l_sugi, 1)
camera:add(tree_l_take, 2)
camera:add(spriteInstance, 3) -- Add player to layer 1 of the camera
camera:add(panning,3,true)
camera:add(moon, 4)
camera:add(background, 5)
camera:track()
--camera:setFocus(spriteInstance)
--camera:prependLayer()
local tPrevious = system.getTimer()

local function move(event)
	
	local tDelta = event.time - tPrevious
	tPrevious = event.time

	local xOffset = ( 0.2 * tDelta )
	spriteInstance.x = spriteInstance.x - (xOffset*spriteInstance.xScale)

	panning.x = panning.x + ( (spriteInstance.x + (cameraXAdditional 					  )  ) - panning.x) * 0.05
  	panning.y = panning.y + ( (spriteInstance.y + (cameraYAdditional            		  )  ) - panning.y) * 0.05
	
end

spriteInstance:play()

Runtime:addEventListener("enterFrame",move)

local myListener = function( event )
    print( "Listener called with event of type: " .. event.name )
    local tDelta = event.time - tPrevious
	tPrevious = event.time

    if(event.x > _W*0.5)then
    	spriteInstance.xScale=-1
    	cameraXAdditional = spriteInstance.width*1.3 
    else
    	spriteInstance.xScale= 1
    	cameraXAdditional =  spriteInstance.width*-1.3 
    end

    
end

Runtime:addEventListener( "touch", myListener )
