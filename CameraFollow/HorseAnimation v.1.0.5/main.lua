-- Project: HorseSprite
--
-- Date: September 27, 2010
--
-- Version: 1.0
--
-- File name: main.lua
--
-- Code type: Game Edition Sample Code
--
-- Author: Japan Corona Group
--
-- Demonstrates: Use of optimized sprite sheets, loaded using newSpriteSheetFromData
--
-- File dependencies: none
--
-- Target devices: Simulator (results in Console)
--
-- Limitations:
--
-- Update History:
--
-- Comments: 
--
-- Sample code is MIT licensed, see https://www.coronalabs.com/links/code/license
-- Copyright (C) 2010 Corona Labs Inc. All Rights Reserved.
--
--	Supports Graphics 2.0
---------------------------------------------------------------------------------------



---------------------------------------------------------------------------------------
-- Camera Perspective code
-- Perspective is a free, easy to use camera library for integrating a 
-- "tracking viewport" into your game.
-- Simply create the camera with perspective.createView(), add objects to it with camera:add(), 
-- and start it tracking with camera:track().
-- Then, you can move your focus object and the camera will track to follow it.
-- 
-- Usage file is found at the Gist
--
-- https://gist.github.com/GymbylCoding/8675733
-- Contributor: 
-- Caleb P (Gymbyl Coding)
--
--
---------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------
-- Additional code for panning rect to follow horse sprite 
-- created by Satwant Singhe Kenth - @SatwantSK
-- with assistance from Alan - @TheBestBigAl
--
--
---------------------------------------------------------------------------------------

local centerX = display.contentCenterX
local centerY = display.contentCenterY
local _W = display.contentWidth
local _H = display.contentHeight

-- Define reference points locations anchor ponts
local TOP_REF = 0
local BOTTOM_REF = 1
local LEFT_REF = 0
local RIGHT_REF = 1
local CENTER_REF = 0.5

display.setStatusBar(display.HiddenStatusBar)

local perspective = require("camera.perspective")

--------------------------------------------------------------------------------
-- Build Camera
--------------------------------------------------------------------------------
local camera = perspective.createView()


display.setDefault( "anchorX", 0.0 )	-- default to TopLeft anchor point for new objects
display.setDefault( "anchorY", 0.0 )


-- The following demonstrates using the new image sheet data format 
-- where uma_old.lua has been migrated to the new format (uma.lua)
local options =
{
	frames = require("uma").frames,
}

-- The new sprite API
local umaSheet = graphics.newImageSheet( "uma.png", options )
local spriteOptions = { name="uma", start=1, count=8, time=1000 }
local spriteInstance = display.newSprite( umaSheet, spriteOptions )

spriteInstance.anchorX = RIGHT_REF
spriteInstance.anchorY = BOTTOM_REF
spriteInstance.x = _W*0.5
spriteInstance.y = _H*0.5
spriteInstance.anchorY=0
spriteInstance.anchorX=0

local background = display.newImage("background.png", 0, 0 )

background.x = -_W
background.y = 0
background.width = _W*3.5
background.anchorY=0
background.anchorX=0 
local moon = display.newImage("moon.png", _W*0.7, 19) 
local tree_l_sugi = display.newImage("tree_l_sugi.png", _W*1, 0) 
local tree_l_take = display.newImage("tree_l_take.png", _W*0.3, 0) 

local panning
panning = display.newRect( spriteInstance.x, spriteInstance.y, spriteInstance.width, spriteInstance.height )
panning.alpha=0.0--should be 0, but increase to see what is being followed

local cameraXAdditional = -spriteInstance.width*1.3 -- the amount the camera follows on x Axis
local cameraYAdditional =  0


local rakkan = display.newImage("rakkan.png", 19, 217) 
local rakkann = display.newImage("rakkann.png", 450, 11) 


camera:setParallax(1.5, 1.1, 1, 0.6, 0.5) -- Set parallax quickly for multiple layers. Each value provided will apply to the correspondingly indexed layer. Provide a table with {x, y} values for an argument to set X and Y parallax independently

local boundaries = {}           
		    boundaries.bx1=0--_W*0.5
        boundaries.bx2= _W*2--levelWidth-_W*0.5
        boundaries.by1=-_H*5
        boundaries.by2=_H*0.55

camera:setBounds(boundaries.bx1,boundaries.bx2, boundaries.by1,boundaries.by2 ) -- Set the bounding box that tracking is confined to. Any values that evaluate to Boolean negative are interpreted as infinite; infinite values apply to an entire axis (if x2 is infinite and x1 is not, X-axis constraint will be disabled)

camera:add(tree_l_sugi, 1)
camera:add(tree_l_take, 2)
camera:add(spriteInstance, 3) -- Add player to layer 1 of the camera
camera:add(panning,3,true)
camera:add(moon, 4)
camera:add(background, 5)
camera:track()

local tPrevious = system.getTimer()

local function move(event)
	
	local tDelta = event.time - tPrevious
	tPrevious = event.time

	local xOffset = ( 0.2 * tDelta )
	spriteInstance.x = spriteInstance.x - (xOffset*spriteInstance.xScale)

	panning.x = panning.x + ( (spriteInstance.x + (cameraXAdditional)  ) - panning.x) * 0.05
  panning.y = panning.y + ( (spriteInstance.y + (cameraYAdditional)  ) - panning.y) * 0.05
	
end

spriteInstance:play()

Runtime:addEventListener("enterFrame",move)

local myListener = function( event )
    print( "Listener called with event of type: " .. event.name )
    local tDelta = event.time - tPrevious
	tPrevious = event.time

    if(event.x > _W*0.5)then
    	spriteInstance.xScale=-1
    	cameraXAdditional = spriteInstance.width*1.3 
    else
    	spriteInstance.xScale= 1
    	cameraXAdditional =  spriteInstance.width*-1.3 
    end

    
end

Runtime:addEventListener( "touch", myListener )
