display.setStatusBar( display.HiddenStatusBar )
local physics = require( "physics" )
physics.start()
local _W = display.contentWidth
local _H = display.contentHeight
display.setDefault( "cameraSource", "back" )
local background 
if ( system.getInfo( "platformName" ) == "Android" or system.getInfo( "platformName" ) == "Mac OS X")then
    background = display.newImage("garden.JPG",_W*0.5, _H*0.5, _W, _H)
else
    background = display.newRect( _W*0.5, _H*0.5, _W, _H )
    background.fill = { type = "camera" }
end
local ground = display.newRect( _W*0.5, _H*1, _W, _H*0.2 )
ground.myName="ground"
physics.addBody( ground,"static" )
local sheetData1 = { width=200, height=300, numFrames=4, sheetContentWidth=800, sheetContentHeight=300 }
local imageSheet = graphics.newImageSheet( "seedSpritesheetTiny.png", sheetData1 )
local sequenceData ={{ name="hit", start=1, count=4,time=120, },{ name="idle", frames={ 1, 3}, time=1000, loopCount=0 },}
local enemy = display.newSprite( imageSheet, sequenceData )
local ball
enemy.x, enemy.y = _W*0.5, _H*0.6
enemy:setSequence( "idle" )
enemy:play()
enemy.myName="enemy"
local onObjectTouch
local restartTimer
local function enemyHit()
    local function vanish()
        transition.to( enemy, { time=1000, alpha = 0, y= ball.y-_H*0.05, x= ball.x } )
    end
    transition.to( enemy, { time=2000, rotation =75, y=ball.y-_H*0.3, x=ball.x,onComplete = vanish } )
end
local function resolveColl( timerRef )
    timer.cancel( restartTimer )
    ball:removeEventListener( "collision" )
    ball.bodyType="static"
end
local function onLocalCollision( self, event )
    if ( event.phase == "began" ) then
        if(event.other.myName=="enemy")then
            local t = timer.performWithDelay( 10, resolveColl, 1 )
            enemyHit()
            enemy:setSequence( "hit" )
            enemy:play() 
        end
    end
end
local function makeNewBall()
   physics.removeBody( enemy ) 
    if(ball)then
       physics.removeBody( ball )
       ball:removeSelf( )
       ball:addEventListener( "touch", ball )
       ball:removeEventListener( "collision" )
    end 
   ball = display.newImage("pokeballTiny.png",_W*0.5,_H*0.9)
   ball.myName="ball"
   physics.addBody( ball,"dynamic", { density=0.5, friction=0.3, bounce=0.2, radius=ball.width*0.25 } )
   ball.touch = onObjectTouch
   ball:addEventListener( "touch", ball )
   ball.collision = onLocalCollision
   ball:addEventListener( "collision" )
end
local function startEnemy()
    physics.addBody( enemy,"static", { density=0.5, friction=0.3, bounce=0.2, radius=enemy.width*0.25 } )
end
local function enemyLive()
    startEnemy()
    restartTimer = timer.performWithDelay( 4000, makeNewBall )
end
local function startAttack() 
    ball:removeEventListener( "touch", ball )
    transition.to( ball, { time=2000, width=ball.width*0.5, height=ball.height*0.5,onComplete=enemyLive } )
end
local startX, startY, endX, endY
onObjectTouch=function( self, event )
    if ( event.phase == "began" ) then
        startX,startY = event.x, event.y
        display.getCurrentStage():setFocus( self )
        self.isFocus = true
    elseif ( self.isFocus ) then
        if ( event.phase == "ended" or event.phase == "cancelled" ) then
            endX,endY = event.x, event.y
            local yAmount, xAmount = endY-startY, endX-startX
            display.getCurrentStage():setFocus( nil )
            self.isFocus = nil
            ball:setLinearVelocity( 1*xAmount, 1*yAmount )
            ball:applyTorque( xAmount )
            startAttack()
        end
    end
    return true
end
makeNewBall()



